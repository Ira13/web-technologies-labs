import {AfterViewInit, Component, ComponentRef, HostListener} from '@angular/core';
import {ToursListComponent} from './tours-list/tours-list.component';
import {TourComponent} from './tour/tour.component';
import {Routes} from './navigatiion/navigation.models';
import {HomeComponent} from './home/home.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  title = 'web-technologies';
  activeComponent?: HomeComponent | TourComponent;

  get hideNavigationAndFooter(): boolean {
    return window.location.pathname.includes('lab');
  }

  ngAfterViewInit(): void {
  }

  onActivate(componentRef: HomeComponent | TourComponent){
    this.activeComponent = componentRef;
  }

  onNavigationChange(route: Routes) {
    if (route === Routes.tours && this.activeComponent instanceof HomeComponent ) {
      this.activeComponent.toursListComponentWrapper?.nativeElement.scrollIntoView({behavior: 'smooth'});
    }
  }
}
