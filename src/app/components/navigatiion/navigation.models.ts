import {ToursListComponent} from '../tours-list/tours-list.component';
import {Component} from '@angular/core';

export enum Routes {
  tours = 'tours'
}
