import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app.component';
import { ToursListComponent } from './components/tours-list/tours-list.component';
import { TourComponent } from './components/tour/tour.component';
import { NavigationComponent } from './components/navigatiion/navigation.component';
import { TopBannerComponent } from './components/top-banner/top-banner.component';
import { HomeComponent } from './components/home/home.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FooterComponent} from './components/footer/footer.component';
import { Lab1DescriptionComponent } from './components/lab1-description/lab1-description.component';
import { ThirdLabComponent } from './components/third-lab/third-lab.component';
import { Lab3TableFixedComponent } from './components/lab3-table-fixed/lab3-table-fixed.component';
import { Lab3TableFlexibleComponent } from './components/lab3-table-flexible/lab3-table-flexible.component';
import { Lab3BlockFixedComponent } from './components/lab3-block-fixed/lab3-block-fixed.component';
import { Lab3BlockFlexibleComponent } from './components/lab3-block-flexible/lab3-block-flexible.component';
import { Lab4CalculatorComponent } from './components/lab4-calculator/lab4-calculator.component';
import { Lab4Task1Component } from './components/lab4-task1/lab4-task1.component';

@NgModule({
  declarations: [
    AppComponent,
    ToursListComponent,
    TourComponent,
    NavigationComponent,
    TopBannerComponent,
    HomeComponent,
    FooterComponent,
    Lab1DescriptionComponent,
    ThirdLabComponent,
    Lab3TableFixedComponent,
    Lab3TableFlexibleComponent,
    Lab3BlockFixedComponent,
    Lab3BlockFlexibleComponent,
    Lab4CalculatorComponent,
    Lab4Task1Component,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
