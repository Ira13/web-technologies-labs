import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-top-banner',
  templateUrl: './top-banner.component.html',
  styleUrls: ['./top-banner.component.scss']
})
export class TopBannerComponent implements OnInit {

  @Output()
  toursClicked = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

  onContactUsClick() {
    window.scrollTo({top: 10000, behavior: 'smooth'});
  }
}
