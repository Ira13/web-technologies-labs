import {Component, EventEmitter, HostListener, OnInit, Output} from '@angular/core';
import {Routes} from './navigation.models';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  @Output()
  itemClicked = new EventEmitter<Routes>;
  Routes = Routes;
  scrolledPastHomeSection = window.location.pathname !== '/';
  private readonly homeOffset = 40;

  @HostListener("window:scroll", [])
  onWindowScroll() {
    const offset = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    if (window.location.pathname !== '/') {
      this.scrolledPastHomeSection = true;
      return;
    }
    if (offset > this.homeOffset) {
      this.scrolledPastHomeSection = true;
    } else {
      this.scrolledPastHomeSection = false;
    }
  }

  scrollToTop() {
    window.scrollTo({top: 0, behavior: 'smooth'});
  }

  onContactUsClick() {
    window.scrollTo({top: 10000, behavior: 'smooth'});
  }


  constructor() { }

  ngOnInit(): void {
  }

}
