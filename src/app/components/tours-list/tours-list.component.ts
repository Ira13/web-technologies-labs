import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {ITour} from '../../models/tour.models';
import {TourService} from '../../services/tour.service';

@Component({
  selector: 'app-tours-list',
  templateUrl: './tours-list.component.html',
  styleUrls: ['./tours-list.component.scss']
})
export class ToursListComponent implements OnInit {

  tours$: Observable<ITour[]> = this.toursService.getTours();

  constructor(
    private toursService: TourService,
  ) { }

  ngOnInit(): void {
  }

}
