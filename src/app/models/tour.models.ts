export interface ITour {
  id: number;
  imgUrl: string;
  title: string;
  description: string;
  availableDates: string[];
  tags: string[];
  price: string;
  route: string;
  startCity: string;
  transport: Transport[];
  durationDays: number;
  plan: string[];
}

export enum Transport {
  Bus,
  Train,
  Plane,
}
