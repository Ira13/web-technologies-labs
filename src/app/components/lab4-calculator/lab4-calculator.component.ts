import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';

enum Operator {
  ADD = '+',
  SUB = '-',
  MUL = '*',
  DIV = '/',
  SQR = 'sqrt',
  POW = 'x^y',
  PER = '%',
  CLR = 'C',
  EQL = '='
}

@Component({
  selector: 'app-lab4-calculator',
  templateUrl: './lab4-calculator.component.html',
  styleUrls: ['./lab4-calculator.component.scss']
})
export class Lab4CalculatorComponent implements OnInit {

  firstOperand = new FormControl<number | null>(null);
  screenControl = new FormControl('0');
  waitingForSecondOperand: boolean = false;
  operator: Operator | undefined;
  valueBtns = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.'];
  verticalActions = [Operator.ADD, Operator.SUB, Operator.MUL, Operator.DIV];
  horizontalActions = [Operator.SQR, Operator.POW, Operator.PER];
  summaryActions = [Operator.CLR, Operator.EQL];
  constructor() { }

  ngOnInit(): void {
  }

  resetCalculator() {
    this.screenControl.setValue('0');
    this.firstOperand.reset();
    this.waitingForSecondOperand = false;
    this.operator = undefined;
  }

  onValueClick(value: string) {
    if (this.waitingForSecondOperand) {
      this.screenControl.setValue(value);
      this.waitingForSecondOperand = false;
    } else {
      this.screenControl.setValue(this.screenControl.value === '0' ? value : this.screenControl.value + value);
    }
  }

  onActionClick(nextOperator: Operator) {
    if (nextOperator === Operator.CLR) {
      this.resetCalculator();
      return;
    }

    const inputValue = parseFloat(this.screenControl.value!);

    if (nextOperator === Operator.PER && !this.operator) {
      this.operator = nextOperator;
      this.waitingForSecondOperand = false;
      this.firstOperand.setValue(inputValue);
    }

    if (this.operator && this.waitingForSecondOperand) {
      this.operator = nextOperator;
      return;
    }
    if (this.firstOperand.value == null) {
      this.firstOperand.setValue(inputValue);
    } else if (this.operator) {
      const currentValue = this.firstOperand.value || 0;
      let result = this.performCalculation(this.operator, currentValue, inputValue);
      this.firstOperand.setValue(result);
      if (nextOperator === Operator.PER) {
        result = this.performCalculation(nextOperator, result, 0);
      }
      this.screenControl.setValue(String(result));
    }
    this.waitingForSecondOperand = true;
    this.operator = nextOperator;

  }

  private performCalculation(operator: Operator, firstOperand: number, secondOperand: number) {
    switch (operator) {
      case Operator.ADD: return firstOperand + secondOperand;
      case Operator.SUB: return firstOperand - secondOperand;
      case Operator.MUL: return firstOperand * secondOperand;
      case Operator.DIV: return firstOperand / secondOperand;
      case Operator.SQR: return Math.sqrt(firstOperand);
      case Operator.POW: return Math.pow(firstOperand, secondOperand);
      case Operator.PER: return firstOperand / 100;
      default: return secondOperand;
    }
  }
}
