import { Component, OnInit } from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-lab4-task1',
  templateUrl: './lab4-task1.component.html',
  styleUrls: ['./lab4-task1.component.scss']
})
export class Lab4Task1Component implements OnInit {

  arrayInput = new FormControl<string>('');
  multipliedArrayOutput = new FormControl<string>('');
  sortedOutput = new FormControl<string>('');
  constructor() { }

  ngOnInit(): void {
  }

  transformArray() {
    const arrayBefore: number[] = this.arrayInput.value?.split(',').map(item => parseFloat(item.trim())) ?? [];
    if (arrayBefore.length === 0) {
      alert('Будь ласка, введіть коректні дані');
      return;
    } else  {
      const multipliedArray = this.multiplyByMaxValue(arrayBefore);
      const sortedArray = this.insertionSort(arrayBefore);
      this.multipliedArrayOutput.setValue(multipliedArray.join(', '));
      this.sortedOutput.setValue(sortedArray.join(', '));
    }
  }

  multiplyByMaxValue(arr: number[]): number[] {
    const maxValue = Math.max(...arr);
    return arr.map(item => item * maxValue);
  }

  insertionSort(arr: number[]): number[] {
    for (let i = 1, l = arr.length; i < l; i++) {
      const current = arr[i];
      let j = i;
      while (j > 0 && arr[j - 1] > current) {
        arr[j] = arr[j - 1];
        j--;
      }
      arr[j] = current;
    }
    return arr;
  };

}
