import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {TourComponent} from './components/tour/tour.component';
import {Lab1DescriptionComponent} from './components/lab1-description/lab1-description.component';
import {ThirdLabComponent} from './components/third-lab/third-lab.component';
import {Lab3TableFixedComponent} from './components/lab3-table-fixed/lab3-table-fixed.component';
import {Lab3TableFlexibleComponent} from './components/lab3-table-flexible/lab3-table-flexible.component';
import {Lab3BlockFixedComponent} from './components/lab3-block-fixed/lab3-block-fixed.component';
import {Lab3BlockFlexibleComponent} from './components/lab3-block-flexible/lab3-block-flexible.component';
import {Lab4CalculatorComponent} from './components/lab4-calculator/lab4-calculator.component';
import {Lab4Task1Component} from './components/lab4-task1/lab4-task1.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'tour/:id',
    component: TourComponent,
  },
  {
    path: 'description',
    component: Lab1DescriptionComponent,
  },
  {
    path: 'lab-3',
    component: ThirdLabComponent,
  },
  {
    path: 'lab-3-table-fixed',
    component: Lab3TableFixedComponent,
  },
  {
    path: 'lab-3-table-flexible',
    component: Lab3TableFlexibleComponent,
  },
  {
    path: 'lab-3-block-fixed',
    component: Lab3BlockFixedComponent,
  },
  {
    path: 'lab-3-block-flexible',
    component: Lab3BlockFlexibleComponent,
  },
  {
    path: 'lab-4-calculator',
    component: Lab4CalculatorComponent,
  },
  {
    path: 'lab-4-task-1',
    component: Lab4Task1Component,
  },
  {
    path: '**',
    component: HomeComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
