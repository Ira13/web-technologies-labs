import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map, Observable} from 'rxjs';
import {ITour} from '../models/tour.models';

@Injectable({
  providedIn: 'root',
})
export class TourService {
  constructor(
    private httpClient: HttpClient,
  ) {
  }

  getTours(): Observable<ITour[]> {
    return this.httpClient.get<ITour[]>('assets/tours.json');
  }

  getTourById(id: string): Observable<ITour | undefined> {
    return this.httpClient.get<ITour[]>('assets/tours.json').pipe(
      map((tours: ITour[]) => tours.find((tour: ITour) => tour.id.toString() === id))
    );
  }
}
