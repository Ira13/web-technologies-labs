import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TourService} from '../../services/tour.service';
import {Observable} from 'rxjs';
import {ITour} from '../../models/tour.models';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-tour',
  templateUrl: './tour.component.html',
  styleUrls: ['./tour.component.scss']
})
export class TourComponent implements OnInit {
  tour$?: Observable<ITour | undefined>;
  form = this.buildForm();

  constructor(
    private activatedRoute: ActivatedRoute,
    private tourService: TourService,
    private fb: FormBuilder,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params['id'];
    this.form.get('tourId')?.setValue(id);
    if (id) {
      this.tour$ = this.tourService.getTourById(id);
    }
  }

  onSubmit(): void {
    if (this.form.valid) {
      this.toastr.success('Ви успішно залишили заявку на тур, наш оператор зателефонує Вам найближчим часом', 'Вітаємо');
    } else {
      this.form.markAllAsTouched();
    }
  }

  getEndDate(startDate: string, durationDays: number): string {
    const splittedDate = startDate.split('.');
    const date = new Date(parseInt('20' + splittedDate[2]), parseInt(splittedDate[1]) - 1, parseInt(splittedDate[0]));
    date.setDate(date.getDate() + durationDays);
    return date.toLocaleDateString().replace(/\//g, '.');
  }

  tourDateSelected(date: string) {
    const dateControl =  this.form.get('date')!;
    dateControl.value === date ? dateControl.setValue('') : dateControl.setValue(date);
  }

  private buildForm() {
    return this.fb.group({
      tourId: [null, Validators.required],
      date: [''],
      name: ['', Validators.required],
      phone: ['', Validators.required],
      comment: [''],
    });
  }
}
