import {Component, ElementRef, OnInit, TemplateRef, ViewChild, ViewRef} from '@angular/core';
import {ToursListComponent} from '../tours-list/tours-list.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @ViewChild('toursListComponentWrapper')
  toursListComponentWrapper?: ElementRef<HTMLElement>;

  constructor() { }

  ngOnInit(): void {
  }

  onToursClicked(): void {
    this.toursListComponentWrapper?.nativeElement.scrollIntoView({behavior: 'smooth'});
  }
}
